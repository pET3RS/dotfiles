" This line makes pacman-installed global Arch Linux vim packages wor.
source /usr/share/nvim/archlinux.vim

source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/vim-plug/coc.vim
source $HOME/.config/nvim/vim-plug/rnvimr.vim
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/themes/airline.vim

