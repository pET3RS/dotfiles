syntax enable			    " Enables syntax highlighting
"set nowrap			        " Display long lines as single line
set encoding=utf-8		    " Sets encoding
set ruler 			        " Show cursor position
set colorcolumn=80		    " Highlight column indicating recomended end of the line
set mouse=a			        " Enable the mouse
set splitbelow			    " Horizontal split will automatically be below
set splitright			    " Vertical split will be automatically on the right
set t_Co=256			    " Support 256 colors
set tabstop=4			    " Set width of actual tab character in spaces
set shiftwidth=4		    " set indent size in spaces
set smarttab
set expandtab			
set smartindent
set autoindent
set number 			        " Line numbers
set updatetime=300		    " faster completion

"colorscheme vim-monokai-tasty
"colorscheme monokai_pro
let g:gruvbox_contrast_dark = 'hard'
colorscheme gruvbox
set background=dark
set noshowmode

" Redo rebind
nnoremap <S-U> :redo<CR>
" Smart moving between split windows
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-H> <C-W>h
nnoremap <C-L> <C-W>l

" Smart resizing of split windows
nnoremap <S-J> <C-W>-
nnoremap <S-K> <C-W>+
nnoremap <S-H> <C-W>>
nnoremap <S-L> <C-W><

" Cycle Tabs
nnoremap <C-[> :tabp<CR>
nnoremap <C-]> :tabn<CR>

" Cycle Buffers
nnoremap <Pageup> :bprevious<CR>
nnoremap <Pagedown> :bnext<CR>

" TeX preview
nnoremap <C-p> :LLPStartPreview<CR>

"set spell
set spelllang=en_us

" Folding
set foldmethod=syntax
set foldlevel=5
set foldnestmax=5
" folds every to level
nnoremap zs :%foldc<CR>    

"doxygen syntax
let g:load_doxygen_syntax=1
let g:doxygen_enhanced_colour=0

"Pdf viewer for live latex preview
let g:livepreview_previewer = 'zathura'
