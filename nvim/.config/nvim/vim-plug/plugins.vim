" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " File Explorer
    Plug 'scrooloose/NERDTree'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Monokai tasty theme
    Plug 'patstockwell/vim-monokai-tasty'
    " Monokai pro theme
    Plug 'phanviet/vim-monokai-pro'
    " Gruvbox theme
    Plug 'morhetz/gruvbox' 
    " status line
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    " Completion
    Plug 'neoclide/coc.nvim', {'branch': 'release'} 
    " Extended highlighting for clangd
    Plug 'jackguo380/vim-lsp-cxx-highlight'
    Plug 'octol/vim-cpp-enhanced-highlight'
    " Ranger in vim
    Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}  
    " Colorizer
    Plug 'lilydjwg/colorizer'
    " multi-cursor
    "Plug 'mg979/vim-visual-multi', {'branch': 'master'}
    " git diff
    Plug 'airblade/vim-gitgutter'
    " markdown syntax
    Plug 'plasticboy/vim-markdown'
    " quick navigation
    Plug 'justinmk/vim-sneak'
    " latex support
    "Plug 'donRaphaco/neotex', { 'for': 'tex' }    
    " A Vim Plugin for Lively Previewing LaTeX PDF Output
    Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
    " editorconfig
    Plug 'editorconfig/editorconfig-vim'
    " vim fugitive
    Plug 'tpope/vim-fugitive'

call plug#end()
