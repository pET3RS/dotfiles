#!/bin/zsh

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

BAR_NAME_MAIN=mainBar
BAR_NAME_SECONDARY=secondaryBar
BAR_CONFIG=$HOME/.config/polybar/config

PRIMARY=$(xrandr --query | grep " connected" | grep "primary" | cut -d" " -f1)
OTHERS=$(xrandr --query | grep " connected" | grep -v "primary" | cut -d" " -f1)

# Launch on primary monitor
MONITOR=$PRIMARY polybar --reload --config=$BAR_CONFIG $BAR_NAME_MAIN &

if [ ! -z $OTHERS ] 
then
    for item in $OTHERS
    do
        MONITOR=$item polybar --reload --config=$BAR_CONFIG $BAR_NAME_SECONDARY &
    done
fi

echo "Bars launched..."
