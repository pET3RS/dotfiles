#!/bin/sh

active_hdmi=$(xrandr -q | grep " connected" | grep "HDMI" | awk '{print $1}')
edp1=$(xrandr -q | grep " connected" | grep "eDP" | awk '{print $1}')
if [[ ! -z "$active_hdmi" && "$active_hdmi" = "HDMI2" ]]; then
	exec xrandr --output eDP1 --mode 1920x1080 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --off --output HDMI2 --primary --mode 1920x1080 --pos 1920x0 --rotate normal
elif [[ ! -z "$active_hdmi" && "$active_hdmi" = "HDMI-2" ]]; then
	exec xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --off --output HDMI-1 --off --output HDMI-2 --primary --mode 1920x1080 --pos 1920x0 --rotate normal
elif [[ ! -z "$edp1" && "$edp1" = "eDP1" ]]; then
	exec xrandr --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --off --output HDMI2 --off
else
	exec xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --off --output HDMI-1 --off --output HDMI-2 --off
fi
